class Dictionary
  attr_reader :entries
  def initialize
    @entries = {}
  end

  def add(entry)
    entry.class == Hash ? @entries.merge!(entry) : @entries[entry] = nil
  end

  def keywords
    @entries.keys.sort
  end

  def include?(keyword)
    keywords.index(keyword)
  end

  def find(str)
    @entries.select { |k| k.include? str }
  end

  def printable
    entry_array = []
    @entries.each_pair { |k, v| entry_array << "[#{k}] \"#{v}\"" }
    entry_array.sort.join("\n")
  end
end
