class Timer
  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    hh = @seconds / 3600
    mm = (@seconds / 60) - (hh * 60)
    ss = @seconds - ((hh * 3600) + (mm * 60))
    time_array = [hh.to_s, mm.to_s, ss.to_s]
    time_array.map! { |i| if i.size == 1 then "0#{i}" else i end }
    time_array.join(':')
  end
end
