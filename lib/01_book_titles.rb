class Book
  attr_accessor :title
  def title=(value)
    little_words = ['a', 'an', 'the', 'in', 'of', 'and']
    words = value.split(' ')
    words.map!.with_index do |i, j|
      if (little_words.include? i) && (j != 0)
        i
      else
        i.capitalize
      end
    end
    @title = words.join(' ')
  end
end
