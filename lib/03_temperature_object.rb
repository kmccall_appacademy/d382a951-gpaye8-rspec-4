class Temperature

  def initialize(options = {})
    @options = options
    complete
  end

  def complete
    if @options.key?(:f)
      @options[:c] = (@options[:f] - 32) * 5 / 9.0
    else
      @options[:f] = @options[:c] * 9 / 5.0 + 32
    end
  end

  def self.from_celsius(n)
    self.new(:c => n)
  end

  def self.from_fahrenheit(n)
    self.new(:f => n)
  end

  def in_celsius
    @options[:c]
  end

  def in_fahrenheit
    @options[:f]
  end
end

class Celsius < Temperature
  def initialize(n, options = {})
    @options = options
    @options[:c] = n
    complete
  end
end

class Fahrenheit < Temperature
  def initialize(n, options = {})
    @options = options
    @options[:f] = n
    complete
  end
end
